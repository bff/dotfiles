" Plugins {{{1
call plug#begin('~/.local/share/nvim/site/plugged')
Plug 'ojroques/vim-oscyank'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'elixir-editors/vim-elixir'
call plug#end()

" Filetypes {{{1
syntax enable
filetype plugin indent on

" Mappings {{{1

"NOTE: mapleader must be set prior to using <leader> in any map commands.
" Apparently, the mapping is resolved using the current leader key at the
" moment the map command is invoked.
let mapleader="\<SPACE>"

" Eliminate the need to press SHIFT for commands
nnoremap ; :

" Relative numbering
function! NumberToggle()
  if(&relativenumber == 1)
    set nornu
    set number
  else
    set rnu
  endif
endfunc
nnoremap <leader>r :call NumberToggle()<cr>

" Disable annoying F1
nnoremap <F1> <NOP>
inoremap <F1> <NOP>
vnoremap <F1> <NOP>

" Use <SPACE>c to clear the highlighting of :set hlsearch.
nnoremap <leader>c :set hlsearch! hlsearch?<CR>

" Use <CTRL+SHIFT>s to reload vimrc
nnoremap <C-S-s> :source ~/.config/nvim/init.vim<cr>:echo 'Reloaded vimrc'<cr>

" Use <SPACE>TAB to toggle a fold
nnoremap <leader><TAB> za

" Terminal Mode Mappings {{{2
:tnoremap <Esc> <C-\><C-n>         " ESC switches back to normal mode when in a terminal buffer
:tnoremap <C-h> <C-\><C-n><C-w>h   " CTRL-h in terminal mode moves left a pane
:tnoremap <C-j> <C-\><C-n><C-w>j   " CTRL-j in terminal mode moves down a pane
:tnoremap <C-k> <C-\><C-n><C-w>k   " CTRL-k in terminal mode moves up a pane
:tnoremap <C-l> <C-\><C-n><C-w>l   " CTRL-l in terminal mode moves right a pane
:nnoremap <C-h> <C-w>h
:nnoremap <C-j> <C-w>j
:nnoremap <C-k> <C-w>k
:nnoremap <C-l> <C-w>l

" Disable arrow keys -- Force myself to learn a vim {{{2
noremap  <Up> :echo 'No arrow keys!'<cr>
noremap  <Down> :echo 'No arrow keys!'<cr>
noremap  <Left> :echo 'No arrow keys!'<cr>
noremap  <Right> :echo 'No arrow keys!'<cr>

" NERDTree {{{1
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader>s :NERDTreeFind<CR>
autocmd StdinReadPre * let s:std_in=1
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let NERDTreeShowHidden=1
let NERDTreeIgnore=['.git$[[dir]]', 'node_modules$[[dir]]', 'newrelic_agent.log', 'build$[[dir]]', 'tmp$[[dir]]']

" Misc {{{1
" Detect linux
let g:os = substitute(system('uname'), '\n', '', '')

" Airline {{{1
" Mac OS specific config
if g:os != 'Linux'
  let g:airline_powerline_fonts = 1
endif


" Vim-GitGutter {{{1
set updatetime=250

" Disable backups {{{1
set noswapfile
set nobackup
set nowritebackup

" Various Interface options {{{1
set number                           " Show line numbers on the left
set nowrap                           " Don't wrap lines
set ruler                            " Show the current row and column
set list listchars=tab:→\ ,trail:·   " Show tab and trailing whitespace characters
set showmode                         " Show current mode
set showcmd                          " Show (partial) command in status line
set cursorline                       " Highlight the currently active line
set cursorcolumn                     " Highlight the currently active column
set mouse=a                          " Enable the mouse in the terminal

" Searching options {{{1
set hlsearch         " Highlight search results
set gdefault         " Add the 'global' (/g) replacement by default
set ignorecase       " Make searching case sensitive normally
set smartcase        " ... unless the query has capital letters.
set incsearch        " Incremental search
" Whitespace formatting {{{1
set smartindent        " Try to keep indenting the newlines based on syntax and context
set tabstop=2          " 1 TAB = 2 spaces
set shiftwidth=2       " ... same as above, don't understand the difference
set expandtab          " Insert spaces when pressing the TAB key
set nojoinspaces       " No spaces after joining lines
set fo=tcrq

" Use <SPACE>q to close the quickfix window
nnoremap <leader>q :ccl<CR>

" Unite {{{1
" TODO - consider removing this?
if !empty(glob("~/.config/nvim/bundle/unite.vim"))
  call unite#filters#matcher_default#use(['matcher_fuzzy'])
endif
nnoremap <leader>y :<C-u>Unite -buffer-name=yank history/yank<cr>

" CtrlP {{{1
" search buffers and cached files simultaneously with SPACE + p 
nnoremap <leader>p :CtrlPMixed<CR>
let g:ctrlp_custom_ignore = {
  \  'dir':  '\v[\/](\.git|\.hg|\.svn|node_modules|tmp|logs)$',
  \ } " Ignore node_modules

" Fugitive {{{1
nnoremap <leader><C-s> :Gstatus<cr>
nnoremap <leader><C-m> :Gcommit<cr>i
nnoremap <leader><C-p> :Gpush<cr>

" markdown {{{1
au FileType markdown setl wrap

" Java {{{1

" Python {{{1
au FileType python setl sw=4 ts=4 et foldmethod=indent foldlevel=99
autocmd FileType python setlocal completeopt-=preview"


" Colorscheme {{{1
" The colorscheme must be set in alacritty.yml (and affects all terminal
" colors)
set t_Co=256
hi CursorLine cterm=None ctermbg=Black
hi CursorColumn ctermfg=None cterm=None ctermbg=Black
hi Todo cterm=Bold ctermfg=Blue
hi NeomakeWarningDefault ctermfg=Yellow cterm=underline
"hi IncSearch ctermbg=Black cterm=Bold
"hi Search ctermbg=Black cterm=Bold




" Clipboard integration across ssh with alacritty {{{1
autocmd TextYankPost * if v:event.operator is 'y' && v:event.regname is '+' | OSCYankReg + | endif

" CoC LSP {{{1

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space><S-s>  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
