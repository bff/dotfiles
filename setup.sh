#!/bin/bash -eu

set -o pipefail

cat <<'ASCIIART'

__/\\\\\\\\\\\\\____/\\\\\\\\\\\\\\\__/\\\\\\\\\\\\\\\_
 _\/\\\/////////\\\_\/\\\///////////__\/\\\///////////__
  _\/\\\_______\/\\\_\/\\\_____________\/\\\_____________
   _\/\\\\\\\\\\\\\\__\/\\\\\\\\\\\_____\/\\\\\\\\\\\_____
    _\/\\\/////////\\\_\/\\\///////______\/\\\///////______
     _\/\\\_______\/\\\_\/\\\_____________\/\\\_____________
      _\/\\\_______\/\\\_\/\\\_____________\/\\\_____________
       _\/\\\\\\\\\\\\\/__\/\\\_____________\/\\\_____________
        _\/////////////____\///______________\///______________

ASCIIART



echo ''
echo '+-------------------------+'
echo '|   1. The Basics         |'
echo '+-------------------------+'
echo ''

./bin/bootstrap-homebrew.sh
./bin/install-bitwarden-cli.sh
type jq || brew install jq || sudo apt-get install -y jq
./bin/describe-device.sh

echo ''
echo '+-------------------------+'
echo '|   2. Setup SSH things   |'
echo '+-------------------------+'
echo ''

mkdir -p ~/.ssh
rm -f ~/.ssh/config && cp ssh_config ~/.ssh/config
./bin/ssh-keygen.sh gitlab bff
./bin/ssh-keygen.sh github brycefisher
./bin/ssh-keygen.sh box.fisher-fleig.me bryce

echo 'Upserting public SSH key to Gitlab.com'
. auth/upsert-gitlab-key.sh


echo '  TODO: * Upload ssh-keys to Github'

echo ''
echo '+-------------------------+'
echo '|   3. Firefox            |'
echo '+-------------------------+'
echo ''

echo '  TODO: * install firefox'
# https://extensionworkshop.com/documentation/enterprise/enterprise-distribution/#controlling-automatic-installations
# https://askubuntu.com/a/1169161
echo '  TODO: * install firefox addons: bitwarden, privacy badger, http everywhere, eno, multi-account-containers'
echo '  TODO: * set default search engine to duckduckgo'

echo ''
echo '+-------------------------+'
echo '|   4. Alacritty          |'
echo '+-------------------------+'
echo ''

type rustup || curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
type alacritty || brew cask install alacritty || sudo snap install alacritty --classic
echo "Install for Linux: https://techviewleo.com/how-to-install-alacritty-terminal-emulator/"
type fish || brew install fish || sudo apt-get -y install fish
brew tap | grep cask-fonts || brew tap homebrew/cask-fonts || echo 'Nope. Need to install fonts'
brew cask list | grep font-fira-code || brew cask install font-fira-code || echo 'TODO: make fonts install'
echo "See: https://github.com/tonsky/FiraCode/wiki/Linux-instructions#installing-with-a-package-manager"
echo 'Configuring alacritty'
mkdir -p ~/.config/alacritty
cat alacritty.yml > ~/.config/alacritty/alacritty.yml

echo ''
echo '+-------------------------+'
echo '|   5. Manage Shell       |'
echo '+-------------------------+'
echo ''

echo '  TODO: * motd'
type rg ||  brew install ripgrep || sudo apt-get install -y ripgrep
type fzf || brew install fzf || sudo apt-get install -y fzf
type delta || brew install git-delta || cargo install git-delta

mkdir -p ~/.config/git
rm -f ~/.config/git/config
cp gitconfig ~/.config/git/config

mkdir -p ~/.config/fish
rm -f ~/.config/fish/config.fish
cp config.fish ~/.config/fish/config.fish

mkdir -p ~/.config/fish/functions/
rm -f ~/.config/fish/functions/fish_prompt.fish
cp fish_prompt.fish ~/.config/fish/functions/

echo '  TODO: * Install nushell'
echo '  TODO: * Install docker or similar'

echo ''
echo '+-------------------------+'
echo '|   6. Configure Neovim   |'
echo '+-------------------------+'
echo ''

type nvim || brew install neovim || sudo apt-get install -y neovim

echo 'Update neovim init.vim'
mkdir -p "$HOME/.config/nvim"
rm -f "$HOME/.config/nvim/init.vim"
cp vimrc "$HOME/.config/nvim/init.vim"

if [ ! -d "$HOME/.local/share/nvim/site/autoload" ]; then
  echo 'Cloning vim plugin manager (vim.plug)'
  curl -fLo "$HOME/.local/share/nvim/site/autoload/plug.vim" --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

echo 'Installing nvim plugins'
nvim -c 'PlugInstall|PlugClean!|qall'

echo ''
echo '+-------------------------+'
echo '|   7. Desktop Apps       |'
echo '+-------------------------+'
echo ''

echo '  TODO: * telegram'
echo '  TODO: * diagrams.net'
echo '  TODO: * keybase'
echo '  TODO: * skype'
echo '  TODO: * gimp'
echo '  TODO: * gpg suite || seahorse seahorse-nautilius on ubutm?
