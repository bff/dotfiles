Dotfiles
--------

_NOTE:_ The canonical version of this project is on [GitLab](https://gitlab.com/bff/dotfiles)


## Objectives:

 - Minimal dependencies
 - Works on OSX and Ubuntu

## Bootstrapping:

```
git clone git@github.com:brycefisher/dotfiles.git
cd dotfiles
./setup.sh
```

## Recommended Follow Up

 - install P4Merge from https://www.perforce.com/product/components/perforce-visual-merge-and-diff-tools

## Similar Repos

 - https://gitlab.com/bff/infra-inventory -- mostly just for listing things out to remember them, not really for executable scripts like this repo