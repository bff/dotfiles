#!/bin/bash -eu

set -o pipefail

function main() {
	type bw && exit 0;

	case $(uname) in
		Linux)
			sudo snap install bw 
			;;
		Darwin)
			brew install bitwarden-cli
			;;
		*)
			echo 'Unsupported'
			exit 1
			;;
	esac
}

main
