#!/bin/bash -eu

set -o pipefail

function keygen() {
  local service=$1
  local username=$2

  if [ ! -d "$HOME/.ssh/$service" ]; then
    echo "Generating $service ssh key"
    mkdir -p "$HOME/.ssh/$service"
    ssh-keygen -b 4096 -f "$HOME/.ssh/$service/$username"
  fi
}

keygen $1 $2
