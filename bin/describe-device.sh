#!/bin/bash -eu

set -o pipefail

source bin/activate.sh

if [[ -e "${DOTFILE_DIR}/device" ]]; then
    echo "This device is called: $(cat "${DOTFILE_DIR}/device")"
else
    read -e -p 'How will you describe this device?'
    echo $REPLY
    echo $REPLY > "${DOTFILE_DIR}/device"
fi
