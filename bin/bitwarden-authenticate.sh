#!/bin/bash -eu

set -o pipefail

status="$(bw status 2>/dev/null | tail -1 | jq -r .status)"
case "$status" in
    "unauthenticated")
        bw login --raw 
        ;;
    "locked")
        bw unlock --raw
        ;;
    *)
        echo "Problem with checking bitwarden status" 1>&2
        exit 1
esac
