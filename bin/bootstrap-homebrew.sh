#!/bin/bash -eu

set -o pipefail

function prerequisites() {
	if [[ ! -d "$(xcode-select -p)" ]]; then
		xcode-select --install
	fi
}

function install() {
	bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
}

function main() {
	if [[ "$(uname)" == 'Darwin' ]]; then
		if [[ -z "$(which brew)" ]]; then
			echo 'Installing homebrew'
			prerequisites
			install
		else
			echo 'homebrew already installed'
		fi
	fi
}

main
