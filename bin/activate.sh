#!/bin/bash -eu

set -o pipefail

export DOTFILE_DIR="${XDG_CONFIG_HOME:-~/.config}/dotfiles"
mkdir -p $DOTFILE_DIR

export GITLAB_USER_ID=292638

export DOTFILE_ACTIVATED=1
