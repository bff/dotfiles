#!/bin/bash

# Setup git autocompletions in bash
source "$HOME/git-completion.bash"
source "$HOME/git-prompt.sh"
export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWSTASHSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_SHOWUPSTREAM="auto"
export PS1='\W$(__git_ps1 " (%s)") \$ '

# Setup aliases I like
alias g='git'
alias n='nvim'
alias vim='nvim'
alias ls='ls -lah'
alias ..='cd ..'

case "$(uname)" in
  'Linux')
    alias open='xdg-open &> /dev/null'
    alias pbcopy='xclip -selection clipboard'
    alias pbpaste='xclip -selection clipboard -o'
    ;;
esac

# Makefile completions for MacOS
# From http://stackoverflow.com/questions/33760647/makefile-autocompletion-on-mac/36044470#36044470
function _makefile_targets {
    local curr_arg;
    local targets;

    # Find makefile targets available in the current directory
    targets=''
    if [[ -e "$(pwd)/Makefile" ]]; then
        targets=$( \
            grep -oE '^[a-zA-Z0-9_-]+:' Makefile \
            | sed 's/://' \
            | tr '\n' ' ' \
        )
    fi

    # Filter targets based on user input to the bash completion
    curr_arg=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -W "${targets[@]}" -- $curr_arg ) );
}
if [ "$(uname)" == "Darwin" ]; then
  complete -F _makefile_targets make
fi

# Setup Path
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="/usr/local/bin:$PATH"
export PATH="./node_modules/.bin:$PATH"

# Remap CapsLock to Control on Linux
if [ "$(uname)" == 'Linux' ]; then
  setxkbmap -option caps:ctrl_modifier
fi

# Pyenv / Pyenv Virtualenv initialization
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PYENV_ROOT="$HOME/.pyenv"
