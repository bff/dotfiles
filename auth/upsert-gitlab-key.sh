#!/bin/bash -eu

function strip_comment() {
    # Soooooo annnoying....gitlab changes the comment around
    # dynamically in an SSH key, so they won't be equal unless
    # you remove the comment

    echo -n "$1" | sed -e 's/ssh-rsa \([^ ]*\) .*/ssh-rsa \1/'
}

function fetch_keys() {
    # Trying to avoid using auth unless absolutely necessary!
    # Looking up ssh public key by user_id doesn't require auth
    strip_comment "$(curl -sS \
      "https://gitlab.com/api/v4/users/${GITLAB_USER_ID}/keys" \
    | jq -r '.[] | .key')"
}

function upload_key() {
    local title="$1"
    local key="$2"

    curl -sS -XPOST \
      -H "Private-Token: $GITLAB_ACCESS_TOKEN" \
      -H 'Content-Type: application/json' \
      -H 'Accepts: application/json' \
      -d "{\"key\": \"${key}\", \"title\": \"${title}\"}" \
      "https://gitlab.com/api/v4/user/keys"
}

function main() {
    . bin/activate.sh

    local gitlab_keys="$(fetch_keys)"
    local public_key="$(strip_comment "$(cat ~/.ssh/gitlab/bff.pub)")"

    if [[ "$gitlab_keys" =~ "$public_key" ]]; then
      echo "Public key already uploaded to Gitlab.com"
    else
      echo "Uploading public key to Gitlab.com"
      local device="$(cat "${DOTFILE_DIR}/device")"
      export BW_SESSION="$(./bin/bitwarden-authenticate.sh)"
      export GITLAB_ACCESS_TOKEN="$(bw get password 'https://api.gitlab.com/')"
      upload_key "$device" "$public_key" &> /dev/null
      unset GITLAB_ACCESS_TOKEN
    fi
}

main
