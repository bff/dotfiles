set PATH ~/.cargo/bin $PATH

alias n="nvim"
alias g="git"
alias gst="git status"
alias gb="git branch"
